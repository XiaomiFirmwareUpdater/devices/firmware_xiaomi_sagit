## Xiaomi Firmware Packages For MI 6 (sagit)

#### Downloads: [![DownloadSF](https://img.shields.io/badge/Download-SourceForge-orange.svg)](https://sourceforge.net/projects/yshalsager/files/Stable) [![DownloadAFH](https://img.shields.io/badge/Download-AndroidFileHost-brightgreen.svg)](https://www.androidfilehost.com/?w=files&flid=268046)

| ID | MIUI Name | Device Name | Codename |
| --- | --- | --- | --- |
| 326 | MI6 | Xiaomi Mi 6 | sagit |
| 326 | MI6Global | Xiaomi Mi 6 Global | sagit |

### XDA Support Thread For sagit:
[Go here](https://forum.xda-developers.com/mi-6/development/firmware-xiaomi-mi-6-t3760924)

### XDA Main Thread:
[Go here](https://forum.xda-developers.com/android/software-hacking/devices-yshalsager-t3741446)

#### by [Xiaomi Firmware Updater](https://github.com/XiaomiFirmwareUpdater)
#### Developer: [yshalsager](https://github.com/yshalsager)
